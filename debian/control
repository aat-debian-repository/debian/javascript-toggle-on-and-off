Source: javascript-toggle-on-and-off
Section: web
Priority: optional
Standards-Version: 4.2.1
Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>
Build-Depends: debhelper (>= 11)
Homepage: http://add0n.com/javascript-toggle.html
Vcs-Git: https://github.com/rNeomy/javascript-toggle-on-and-off.git
Vcs-Browser: https://github.com/rNeomy/javascript-toggle-on-and-off

Package: webext-javascript-toggle-on-and-off-chromium
Architecture: all
Depends: ${misc:Depends}, ${webext:Depends}
Recommends: ${webext:Recommends}, chromium
Provides: ${webext:Provides}
Enhances: ${webext:Enhances}
Replaces: ${webext:Replaces}
Breaks: ${webext:Breaks}
Conflicts: ${webext:Conflicts}
Description: Lightweight method to turn JavaScript on and off.
 The "Javascript Toggle On and Off" extension uses a bulletproof
 method to turn JavaScript execution of the entire browser on
 and off simply through a toolbar button.
 .
 When the add-on is enabled, JavaScript will be denied in both
 normal and private (incognito) modes.
 To allow JavaScript temporary, just click the toolbar button
 once more.

Package: webext-javascript-toggle-on-and-off-firefox
Architecture: all
Depends: ${misc:Depends}, ${webext:Depends}
Recommends: ${webext:Recommends}, firefox-esr (>= 58) | firefox (>= 58)
Provides: ${webext:Provides}
Enhances: ${webext:Enhances}
Replaces: ${webext:Replaces}
Breaks: ${webext:Breaks}
Conflicts: ${webext:Conflicts}
Description: Lightweight method to turn JavaScript on and off.
 The "Javascript Toggle On and Off" extension uses a bulletproof
 method to turn JavaScript execution of the entire browser on
 and off simply through a toolbar button.
 .
 When the add-on is enabled, JavaScript will be denied in both
 normal and private (incognito) modes.
 To allow JavaScript temporary, just click the toolbar button
 once more.
